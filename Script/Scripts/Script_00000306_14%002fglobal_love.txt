module Global
  class Game_System
    # @return [ActorLove]
    attr_reader :actor_love
    def initialize
      @actor_love = ActorLove.new
    end
  end

  class ActorLove
    def initialize
      make_love_data
    end

    def [](index)
      @data[actor_index(index)]
    end

    def []=(index, other)
      @data[actor_index(index)] = other
    end

    private

    def actor_index(index)
      actor = $data_actors[index]
      return actor.original_persona_id if actor && actor.persona_kind == :sub

      index
    end

    def make_love_data
      @data = []
      a = (1..99).map { |i| load_love_data(i) }.compact

      ([0] * 1000).zip(*a).map(&:max).each.with_index do |love, index|
        @data[index] = love
      end
    end

    def load_love_data(index)
      return nil unless File.exist?(DataManager.make_filename(index))

      result = nil
      File.open(DataManager.make_filename(index), "rb") do |file|
        Marshal.load(file)
        v = Marshal.load(file)[:variables]
        result = (1..$data_actors.size).map do |i|
          v.nw_array_get(NWConst::Var::ACTOR_REL_BASE + i) || 0
        end
        result = [0] + result
      end
      result
    end
  end
end
