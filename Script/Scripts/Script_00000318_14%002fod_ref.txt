class Game_BattlerBase
  def od_movable?
    !($game_party.in_over_drive? && $game_party.od_user != self && !ignore_over_drive?)
  end
end
