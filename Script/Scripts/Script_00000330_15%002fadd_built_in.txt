class Array
  def include_any?(array)
    i = 0
    while i < size
      return true if array.include?(self[i])

      i += 1
    end
    false
  end

  def include_all?(array)
    i = 0
    while i < size
      return false unless array.include?(self[i])

      i += 1
    end
    true
  end
end

class Class
  def subclasses_deep
    ObjectSpace.each_object(Class).select { |klass| klass < self }
  end

  def subclasses
    ObjectSpace.each_object(Class).select { |klass| klass.superclass == self }
  end
end

class Integer
  def give_unit
    return "0" if zero?

    sign = self < 0 ? "-" : ""
    unit = ["", ",", ",", ",", ",", ",", ",", ",", ",", ",", ",", ",", ",", ",", ",", ",", ",", ","]
    numbers = []
    unit_value = ""
    abs.to_s.each_char { |num| numbers << num }
    numberblocks = numbers.reverse.each_slice(3).to_a
    numberblocks.each_with_index do |block, index|
      str = ""
      block.reverse.each do |num|
        str += num if num != "0" || str != ""
      end
      str += unit[index] if str != ""
      unit_value = str + unit_value
    end
    sign + unit_value
  end
end

class Integer
  def give_unit_floor(digit)
    data = give_unit.scan(/(\d+(?:\W+|$))/).flatten
    digit -= data[0].width
    data.inject do |s, unit|
      digit -= unit.width
      break s if digit <= 0

      s += unit
      s
    end
  end

  def digit
    Math.log10(abs)
  end
end

class Object
  def to_a
    [self]
  end

  def instance_variable_set_in_hash(hash)
    hash.each do |key, value|
      instance_variable_set("@#{key}", value)
    end
    self
  end

  def itself
    self
  end
end

class Hash
  def transform_values
    return to_enum(:transform_values) { size } unless block_given?

    h = {}
    each do |key, value|
      h[key] = yield value
    end
    h
  end
end

class String
  def width
    length + chars.reject(&:ascii_only?).length
  end
end
